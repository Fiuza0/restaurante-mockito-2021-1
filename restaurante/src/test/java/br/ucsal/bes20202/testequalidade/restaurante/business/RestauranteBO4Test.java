package br.ucsal.bes20202.testequalidade.restaurante.business;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import br.ucsal._20211.testequalidade.restaurante.business.RestauranteBO;
import br.ucsal._20211.testequalidade.restaurante.domain.Comanda;
import br.ucsal._20211.testequalidade.restaurante.domain.Item;
import br.ucsal._20211.testequalidade.restaurante.enums.SituacaoComandaEnum;
import br.ucsal._20211.testequalidade.restaurante.exception.ComandaFechadaException;
import br.ucsal._20211.testequalidade.restaurante.exception.MesaOcupadaException;
import br.ucsal._20211.testequalidade.restaurante.exception.RegistroNaoEncontrado;
import br.ucsal._20211.testequalidade.restaurante.persistence.ComandaDao;
import br.ucsal.bes20202.testequalidade.restaurante.business.builder.ComandaBuilder;
import br.ucsal.bes20202.testequalidade.restaurante.business.builder.ItemBuilder;
@RunWith(MockitoJUnitRunner.class)
public class RestauranteBO4Test {

	/**
	 * Método a ser testado:
	 * 
	 * public Double fecharComanda(Integer codigoComanda) throws
	 * RegistroNaoEncontrado, ComandaFechadaException
	 * 
	 * Verificar se o fechamento de uma comanda ABERTA retorna o valor total e a
	 * situaçao da mesma muda para FECHADA.
	 * 
	 * Atenção:
	 * 
	 * 1. Crie um builder para instanciar as classes Item;
	 * 
	 * 2. Você deve mocar a classe Comanda, além da classe ComandaDao, pois o método
	 * calcularTotal apresenta lógica complexa, não devendo ser testado neste teste
	 * unitário do RestauranteBO.
	 * 
	 */
	private Comanda comandaMock;
	private ComandaDao comandaDaoMock;
	private Item itemMock;
	private Comanda comanda;
	@InjectMocks
	private RestauranteBO restauranteBO;
	
	@BeforeEach
	void setup() throws RegistroNaoEncontrado{
		itemMock = Mockito.mock(Item.class);
		comandaMock = Mockito.mock(Comanda.class);
		comandaDaoMock = Mockito.mock(ComandaDao.class);
		Mockito.when(comandaDaoMock.obterPorCodigo(comanda.getCodigo())).thenReturn(comanda);
		Mockito.when(comandaDaoMock.criarComanda(comanda.getMesa())).thenReturn(comanda);
		Mockito.when(comandaMock.calcularTotal()).thenReturn(comanda.calcularTotal());
	}
	@Test
	public void testarFecharComandaAberta() throws RegistroNaoEncontrado, ComandaFechadaException, MesaOcupadaException {
		comanda = new ComandaBuilder().comSituacaoAberta(SituacaoComandaEnum.ABERTA).Build();
		restauranteBO.abrirComanda(comanda.getMesa().getNumero());
		Item item1 = new ItemBuilder().comNomeEValor("Fil�",21.37).Build();
		comanda.incluirItem(item1, 3);
		Double valorAtual = restauranteBO.fecharComanda(comanda.getCodigo());
		Double valorEsperado = 64.11;
		Assertions.assertEquals(SituacaoComandaEnum.FECHADA, comanda.getSituacao());
		Assertions.assertEquals(valorEsperado,valorAtual);
	}
}
