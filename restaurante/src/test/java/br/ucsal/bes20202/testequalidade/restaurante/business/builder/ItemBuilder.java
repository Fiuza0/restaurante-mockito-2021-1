package br.ucsal.bes20202.testequalidade.restaurante.business.builder;

import java.util.ArrayList;

import br.ucsal._20211.testequalidade.restaurante.domain.Item;

public class ItemBuilder {
	
	private String _nome;
	private Double _valorUnitario;
	
	public Item Build() {
		return new Item(_nome,_valorUnitario);
	}
	
	public ItemBuilder comNome(String nome) {
		_nome = nome;
		return this;
	}
	public ItemBuilder comValor(Double valorUnitario) {
		_valorUnitario = valorUnitario;
		return this;
	}
	public ItemBuilder comNomeEValor(String nome,Double valorUnitario) {
		_valorUnitario = valorUnitario;
		_nome = nome;
		return this;
	}
	public ItemBuilder comValorNegativo(String nome) {
		_valorUnitario = -2d;
		_nome = nome;
		return this;
	}
}
