package br.ucsal.bes20202.testequalidade.restaurante.business.builder;

import br.ucsal._20211.testequalidade.restaurante.domain.Comanda;
import br.ucsal._20211.testequalidade.restaurante.domain.Mesa;
import br.ucsal._20211.testequalidade.restaurante.enums.SituacaoComandaEnum;
import br.ucsal._20211.testequalidade.restaurante.persistence.ComandaDao;

public class ComandaBuilder {
	private SituacaoComandaEnum _situacao;
	private Mesa _mesa = new Mesa();
	
	public Comanda Build() {
		Comanda comanda = new Comanda(_mesa);
		comanda.setSituacao(_situacao);
		return comanda;
	}
	
	public ComandaBuilder comSituacaoAberta(SituacaoComandaEnum situacao) {
		_situacao = situacao;
		return this;
	}
}
